import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { LoginService } from './login/login.service';
import {
  FanProject,
  MultiDisplayControlData,
  FanResult,
  FanSeries
} from './app.common';
import { UUID } from 'angular2-uuid';
import { Http, Headers, Jsonp } from '@angular/http';

@Injectable()
export class FanProjectService {
  readonly url: string;
  constructor(
    private http: HttpClient,
    private userService: LoginService,
    private http1: Http,
    private jsonp: Jsonp
  ) {}

  // getHeaders() {
  //   return new Headers({
  //     Authorization: 'bearer' + ' ' + this.userService.getToken(),
  //     'Content-Type': 'application/json'
  //   });
  // }

  getAllProjects(): Promise<FanProject[]> {
    return this.http
      .get<FanProject[]>('http://reitzindia.in/ReitzService/api/FanProject', {})
      .toPromise();
  }

  getMultiDisplayControlData() {
    return this.http
      .get<MultiDisplayControlData[]>(
        'http://reitzindia.in/ReitzService/api/MultiDisplayControlData',
        {}
      )
      .toPromise();
  }

  submitFanProject(project: FanProject) {
    const data = {
      ...project,
      EntityKey: {
        $id: '8',
        EntityContainerName: 'FanalytixEntities',
        EntityKeyValues: [
          {
            Key: 'Id',
            Type: 'System.Guid',
            Value: 'dfb17e29-a606-4116-8a2b-9350288f061f'
          }
        ],
        EntitySetName: 'FanProjects'
      },
      FanAssemblies: [
        {
          ...project.FanAssemblies,
          $id: '2',
          PressureDifference: project.FanAssemblies.Pressure_Difference,
          EntityKey: {
            $id: '3',
            EntityContainerName: 'FanalytixEntities',
            EntityKeyValues: [
              {
                Key: 'Id',
                Type: 'System.Guid',
                Value: 'a190892d-b082-4a09-9bb4-62090e8a3b56'
              }
            ],
            EntitySetName: 'FanAssemblies'
          },
          FanProject: {
            $ref: '1'
          }
        }
      ],

      GasDatas: [
        {
          ...project.GasDatas,
          $id: '4',
          DptUnit: 1,
          VpUnit: 1,
          VpUnits: 'M3/S',
          FanProject: {
            $ref: '1'
          },
          EntityKey: {
            $id: '5',
            EntityContainerName: 'FanalytixEntities',
            EntityKeyValues: [
              {
                Key: 'Id',
                Type: 'System.Guid',
                Value: '851d012e-eb0a-4757-a324-a3bf929ef057'
              }
            ],
            EntitySetName: 'GasDatas'
          },
          GasOperatingPoints: project.GasOperatingPoints.map(x => ({
            ...x,
            $id: '9',
            Vi: '0.23e-4',
            GasData: {
              $ref: '4'
            },
            EntityKey: {
              $id: '10',
              EntityContainerName: 'FanalytixEntities',
              EntityKeyValues: [
                {
                  Key: 'Id',
                  Type: 'System.Guid',
                  Value: 'c5f5f9cb-0f35-4f8d-b57a-c8c0ab12841b'
                }
              ],
              EntitySetName: 'GasOperatingPoints'
            }
          }))
        }
      ],
      Noises: [],
      MaterialDriveControls: [
        {
          ...project.MaterialDriveControls,
          $id: '6',
          FanProject: {
            $ref: '1'
          },
          RestrictedFanSpeed: null,
          Hertz: null,
          MaterialDensity: null,
          MotorSpeed: null,
          IVCPosition: false,
          EntityKey: {
            $id: '7',
            EntityContainerName: 'FanalytixEntities',
            EntityKeyValues: [
              {
                Key: 'Id',
                Type: 'System.Guid',
                Value: '369184a1-af20-43a4-a9be-872af1c255b9'
              }
            ],
            EntitySetName: 'MaterialDriveControls'
          },

          FanProjectId: '7c5ef204-5857-4bdd-957b-678d466cd00f'
        }
      ],
      UnitValueForInputFields: []
    };

    // this.jsonp.post()

    return this.http1
      .post(
        'http://reitzindia.in/ReitzCalculationService/api/Values',
        { ...data, FanTypes: [] },
        {
          headers: new Headers({
            'Content-Type': 'application/json'
          })
        }
      )
      .toPromise()
      .then(res => res.json() as FanResult[]);
  }
  getFanSeries() {
    return this.http
      .get<FanSeries[]>(
        'http://reitzindia.in/ReitzService/api/FanSeries/14',
        {}
      )
      .toPromise();
  }
}
