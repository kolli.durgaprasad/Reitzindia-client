import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FanProject, GasOperatingPoint } from '../app.common';

@Component({
  selector: 'app-operating-point',
  templateUrl: './operating-point.component.html',
  styleUrls: ['./operating-point.component.css']
})
export class OperatingPointComponent implements OnInit {
  is_edit = false;
  is_edits = false;
  normalDensity = 1.293;
  currentUnits = 'm3/s';
  currentDpt = 'Pa';
  Rv = 461.495;
  Rd = 287.05;
  A = 273.15;
  C0 = 6.1078;

  @Input() selectedProject: FanProject;
  @Output() goToPrevious = new EventEmitter<void>();
  @Output() goToNext = new EventEmitter<void>();
  @Output() addOperatingPoint = new EventEmitter<void>();
  constructor() {}
  ngOnInit() {}

  altitudeCal(temp: number, relative: number, dpt: number) {
    console.log(temp, relative, dpt);

    const T = temp + this.A;
    const pow1 = 7.5 * temp;
    const pow2 = 237.3 + temp;
    const powCal = pow1 / pow2;
    const Pv = this.C0 * Math.pow(10, powCal);

    const PvCal = Pv * 100;
    const PvFinal = Math.round(PvCal * (relative / 100));

    const Ro = this.Rd * T + PvFinal / (this.Rv * T);
    return Ro;
  }
  isDisabled(): boolean {
    return this.is_edit;
  }
  isDisableds(): boolean {
    return this.is_edits;
  }
  toggleEditable(event) {
    if (event.target.checked) {
      this.is_edit = true;
      this.isDisabled();
      if (this.is_edit === true) {
        this.selectedProject.GasOperatingPoints = this.selectedProject.GasOperatingPoints.map(
          r => {
            const Ro = this.altitudeCal(r.T, r.F, r.Dpt);
            return { ...r, Ro: Ro };
          }
        );
      }
    } else {
      if (!event.target.checked) {
        this.is_edit = false;
        this.isDisabled();
      }
    }
  }
  toggleEditables(event) {
    if (event.target.checked) {
      this.is_edits = true;
      this.isDisableds();
    } else {
      if (!event.target.checked) {
        this.is_edits = false;
        this.isDisableds();
      }
    }
  }

  handleEnterKey(event, i, nextTextBox) {
    console.log(i);
    event.preventDefault();
    const nextId = nextTextBox + i;
    console.log(nextId);
    if (!this.is_edit && nextTextBox === 'altitude') {
      const newId = 'humidity' + i;
      document.getElementById(newId).focus();
    }
    if (!this.is_edits && nextTextBox === 'dustLoad') {
      const newIdFirst = 'normalDensity' + i;

      document.getElementById(newIdFirst).focus();
    }
    if (this.is_edit && nextTextBox === 'density') {
      const newIdSecond = 'dynamicViscosity' + i;
      document.getElementById(newIdSecond).focus();
    }
    if (nextTextBox === 'next') {
      document.getElementById(nextTextBox).click();
    } else {
      document.getElementById(nextId).focus();
    }
  }

  convert(value: number, unitsFrom: string, unitsTo: string, density?: number) {
    let vp = value;
    if (unitsTo === 'm3/h') {
      if (unitsFrom === 'm3/m') {
        vp = vp / 60;
      }
      if (unitsFrom === 'm3/s') {
        vp = vp / (60 * 60);
      }
      if (unitsFrom === 'Nm3/h') {
        vp = vp * 1.293 / density;
      }
      if (unitsFrom === 'Nm3/s') {
        vp = this.convert(
          this.convert(value, 'Nm3/s', 'm3/s', density),
          'm3/s',
          'm3/h'
        );
      }
      if (unitsFrom === 'Nm3/m') {
        vp = this.convert(
          this.convert(value, 'Nm3/m', 'm3/m', density),
          'm3/m',
          'm3/h'
        );
      }
    }
    if (unitsTo === 'm3/s') {
      if (unitsFrom === 'm3/m') {
        vp = vp * 60;
      }
      if (unitsFrom === 'm3/h') {
        vp = vp * 60 * 60;
      }
      if (unitsFrom === 'Nm3/s') {
        vp = vp * 1.293 / density;
      }
      if (unitsFrom === 'Nm3/m') {
        vp = this.convert(
          this.convert(value, 'Nm3/m', 'm3/m', density),
          'm3/m',
          'm3/s'
        );
      }
      if (unitsFrom === 'Nm3/h') {
        vp = this.convert(
          this.convert(value, 'Nm3/m', 'm3/h', density),
          'm3/h',
          'm3/s'
        );
      }
    }
    if (unitsTo === 'm3/m') {
      if (unitsFrom === 'm3/h') {
        vp = vp * 60;
      }
      if (unitsFrom === 'm3/s') {
        vp = vp / 60;
      }
      if (unitsFrom === 'Nm3/m') {
        vp = vp * 1.293 / density;
      }
      if (unitsFrom === 'Nm3/h') {
        vp = this.convert(
          this.convert(value, 'Nm3/h', 'm3/h', density),
          'm3/h',
          'm3/m'
        );
      }
      if (unitsFrom === 'Nm3/s') {
        vp = this.convert(
          this.convert(value, 'Nm3/s', 'm3/s', density),
          'm3/s',
          'm3/m'
        );
      }
    }
    if (unitsTo === 'Nm3/h') {
      if (unitsFrom === 'm3/h') {
        vp = value * density / 1.293;
      }

      if (unitsFrom === 'm3/s') {
        vp = this.convert(
          this.convert(vp, 'm3/s', 'm3/h'),
          'm3/h',
          'Nm3/h',
          density
        );
      }
      if (unitsFrom === 'm3/m') {
        vp = this.convert(
          this.convert(vp, 'm3/m', 'm3/h'),
          'm3/h',
          'Nm3/h',
          density
        );
      }
      if (unitsFrom === 'Nm3/m') {
        vp = this.convert(
          this.convert(vp, 'Nm3/m', 'm3/m'),
          'm3/m',
          'Nm3/h',
          density
        );
      }
      if (unitsFrom === 'Nm3/s') {
        vp = this.convert(
          this.convert(vp, 'Nm3/s', 'm3/s'),
          'm3/s',
          'Nm3/h',
          density
        );
      }
    }
    if (unitsTo === 'Nm3/s') {
      if (unitsFrom === 'm3/s') {
        vp = value * density / 1.293;
      }
      if (unitsFrom === 'm3/m') {
        vp = this.convert(
          this.convert(vp, 'm3/m', 'm3/s'),
          'm3/s',
          'Nm3/s',
          density
        );
      }
      if (unitsFrom === 'm3/m') {
        vp = this.convert(
          this.convert(vp, 'm3/m', 'm3/s'),
          'm3/s',
          'Nm3/s',
          density
        );
      }
      if (unitsFrom === 'Nm3/m') {
        vp = this.convert(
          this.convert(vp, 'Nm3/m', 'm3/m'),
          'm3/m',
          'Nm3/s',
          density
        );
      }
      if (unitsFrom === 'Nm3/h') {
        vp = this.convert(
          this.convert(vp, 'Nm3/h', 'm3/h'),
          'm3/h',
          'Nm3/s',
          density
        );
      }
    }
    if (unitsTo === 'Nm3/m') {
      if (unitsFrom === 'm3/m') {
        vp = value * density / 1.293;
      }
      if (unitsFrom === 'm3/h') {
        vp = this.convert(
          this.convert(vp, 'm3/h', 'm3/m'),
          'm3/m',
          'Nm3/m',
          density
        );
      }
      if (unitsFrom === 'm3/s') {
        vp = this.convert(
          this.convert(vp, 'm3/s', 'm3/m'),
          'm3/m',
          'Nm3/m',
          density
        );
      }
      if (unitsFrom === 'Nm3/h') {
        vp = this.convert(
          this.convert(vp, 'Nm3/h', 'm3/h', density),
          'm3/h',
          'Nm3/m',
          density
        );
      }
      if (unitsFrom === 'Nm3/s') {
        vp = this.convert(
          this.convert(vp, 'Nm3/s', 'm3/s', density),
          'm3/s',
          'Nm3/m',
          density
        );
      }
    }
    return vp;
  }

  selectChangeVp(event) {
    this.selectedProject.GasOperatingPoints = this.selectedProject.GasOperatingPoints.map(
      e => {
        const vp = this.convert(
          e.Vp,
          this.currentUnits,
          event.target.value,
          e.Ro
        );
        return { ...e, Vp: vp };
      }
    );
    this.currentUnits = event.target.value;
  }
  convertDpt(value: number, unitsFrom: string, unitsTo: string) {
    console.log(value, unitsFrom, unitsTo);
    let dpt = value;
    if (unitsFrom === 'Pa') {
      if (unitsTo === 'daPa') {
        dpt = dpt / 10;
      }
      if (unitsTo === 'mbar') {
        dpt = dpt / 100;
      }
      if (unitsTo === 'mmWG') {
        dpt = dpt / 9.806805923310778;
      }
    }
    if (unitsFrom === 'daPa') {
      if (unitsTo === 'Pa') {
        dpt = dpt * 10;
      }
      if (unitsTo === 'mbar') {
        dpt = dpt / 10;
      }
      if (unitsTo === 'mmWG') {
        dpt = dpt / 0.9806805923310777;
      }
    }
    if (unitsFrom === 'mbar') {
      if (unitsTo === 'Pa') {
        dpt = dpt * 100;
      }
      if (unitsTo === 'daPa') {
        dpt = dpt * 10;
      }
      if (unitsTo === 'mmWG') {
        dpt = dpt * 10.197;
      }
    }
    if (unitsFrom === 'mmWG') {
      if (unitsTo === 'Pa') {
        dpt = Math.round(dpt * 9.806805923310778);
      }
      if (unitsTo === 'daPa') {
        dpt = Math.round(dpt * 0.9806805923310777);
      }
      if (unitsTo === 'mbar') {
        dpt = Math.round(dpt * 0.09806805923310778);
      }
    }
    return dpt;
  }

  selectChangeDpt(event) {
    console.log(event.target.value);
    this.selectedProject.GasOperatingPoints = this.selectedProject.GasOperatingPoints.map(
      s => {
        const dpt = this.convertDpt(s.Dpt, this.currentDpt, event.target.value);
        return { ...s, Dpt: dpt };
      }
    );
    this.currentDpt = event.target.value;
  }
}
