import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpellerScantllingsComponent } from './impeller-scantllings.component';

describe('ImpellerScantllingsComponent', () => {
  let component: ImpellerScantllingsComponent;
  let fixture: ComponentFixture<ImpellerScantllingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpellerScantllingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpellerScantllingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
