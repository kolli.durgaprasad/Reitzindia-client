import { Component, OnInit } from '@angular/core';
import {
  FanProject,
  GasOperatingPoint,
  FanAssembly,
  MultiDisplayControlData,
  MaterialDriveControl
} from '../app.common';
import { FanProjectService } from '../fanproject.service';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent implements OnInit {
  multiDisplayControlData: MultiDisplayControlData[];
  userName: string;
  currentTab = '';
  constructor(private service: FanProjectService) {}

  materialControl: MaterialDriveControl = {
    $id: '',
    BackBlades: true,
    Control: 0,
    DesignType: '',
    Drive: '',
    FanProjectId: '',
    FanType: '',
    IECStandardMotor: true,
    IVCPosition: '',
    Id: '',
    InletOutletDuct: 0,
    MaterialDensity: 0,
    MaterialName: '',
    MaterialYieldStrength: 0,
    MechanicalDesignTemperature: 0,
    MotorPower: 0,
    MotorSpeed: 0,
    NoiseDataRequired: true,
    NominalMotorSpeed: 0,
    StandardImpellerMaterial: true,
    Width: 0
  };

  fanAssembly: FanAssembly = {
    $id: '',
    EvaseOutlet_InletAreaRatio: 0,
    FanProjectId: '',
    Id: '',
    InletBox: 0,
    InletOtherParts: 0,
    InletSoundSilencer: 0,
    OutletOtherParts: 0,
    OutletSilencer: 0,
    Pressure_Difference: 0
  };

  selectedProject: FanProject = {
    $id: '',
    CustomerName: '',
    Date: new Date(),
    Engineer: '',
    FanAssemblies: this.fanAssembly,
    FanProjectId: '',
    FanType: '',
    GasDatas: undefined,
    GasOperatingPoints: [],
    LastUiFormCompleted: '',
    MaterialDriveControls: this.materialControl,
    Noises: undefined,
    ProjectName: '',
    Proposal_OrderNo: ''
  };

  ngOnInit() {
    this.userName = localStorage.getItem('userName');
    this.service
      .getMultiDisplayControlData()
      .then(x => (this.multiDisplayControlData = x));
  }

  changeTab(tabName: string) {
    console.log(this.selectedProject);
    this.currentTab = tabName;
  }

  onAddOperatingPoint() {
    const op: GasOperatingPoint = {
      $id: '',
      Dpt: 0,
      F: 0,
      GasDataId: '',
      Id: '',
      P1: 0,
      Ro: 0,
      T: 0,
      Vi: 0,
      Vp: 0
    };
    const operatingPoints = [...this.selectedProject.GasOperatingPoints, op];
    this.selectedProject = {
      ...this.selectedProject,
      GasOperatingPoints: operatingPoints
    };
  }
}
