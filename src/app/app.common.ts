import { INFERRED_TYPE } from '@angular/compiler/src/output/output_ast';

export interface Credentials {
  userName: string;
  password: string;
}

export interface User {
  $id: string;
  Claims?: any[];
  Logins?: any[];
  Roles: [{ $ref: string }];
  Id: string;
  UserName: string;
  PasswordHash: string;
  SecurityStamp: string;
}

export interface Role {
  $id: string;
  UserId: string;
  RoleId: string;
  Role: { $id: string; Id: string; Name: string };
  User: User;
}

export interface LoginResponse {
  $id: string;
  token: string;
  user: {
    $id: string;
    UserId: string;
    UserName: string;
    Roles: Role[];
  };
}

export interface FanAssembly {
  $id: string;
  EvaseOutlet_InletAreaRatio: number;
  FanProjectId: string;
  Id: string;
  InletBox: number;
  InletOtherParts: number;
  InletSoundSilencer: number;
  OutletOtherParts: number;
  OutletSilencer: number;
  Pressure_Difference: number;
}

export interface GasData {
  $id: string;
  BarometricPressure_Elevation: number;
  DptUnit: string;
  DptUnits: string;
  FanProjectId: string;
  GasDustload: number;
  Id: string;
  VpUnit: string;
  VpUnits: string;
}

export interface GasOperatingPoint {
  $id: string;
  Dpt: number;
  F: number;
  GasDataId: string;
  Id: string;
  P1: number;
  Ro: number;
  T: number;
  Vi: number;
  Vp: number;
}
export interface MaterialDriveControl {
  $id: string;
  BackBlades: boolean;
  Control: number;
  DesignType?: string;
  Drive?: string;
  FanProjectId: string;
  FanType: string;
  IECStandardMotor: boolean;
  IVCPosition: string;
  Id: string;
  InletOutletDuct: number;
  MaterialDensity: number;
  MaterialName: string;
  MaterialYieldStrength: number;
  MechanicalDesignTemperature: number;
  MotorPower: number;
  MotorSpeed: number;
  NoiseDataRequired: boolean;
  NominalMotorSpeed?: number;
  StandardImpellerMaterial: boolean;
  Width: number;
}

export interface FanProjectNew {
  $id: string;
  CustomerName: string;
  Date: Date;
  Engineer: string;
  Id: string;
  LastUiFormCompleted?: string;
  ProjectName: string;
  Proposal_OrderNo: string;
}

export interface Noise {
  $id: string;
  BackgroundNoiseCorrection: number;
  Design: number;
  DistanceBetweenStiffners: number;
  FanLocation: number;
  FanProjectId: string;
  FanProjectNew: FanProjectNew;
  HousingMaterial: number;
  HousingMetalPlateThickness: number;
  Id: string;
  RoomAbsorptionArea: number;
}
export interface FanProject {
  $id: string;
  CustomerName: string;
  Date: Date;
  Engineer: string;
  FanAssemblies: FanAssembly;
  FanProjectId: string;
  FanType?: string;
  GasDatas: GasData;
  GasOperatingPoints: GasOperatingPoint[];
  LastUiFormCompleted?: string;
  MaterialDriveControls: MaterialDriveControl;
  Noises: Noise;
  ProjectName: string;
  Proposal_OrderNo: string;
}

export interface MultiDisplayControlData {
  $id: string;
  Name: string;
  DisplayText: string;
  Value: string;
  ParentId: number;
  Id: number;
  Type: number;
  ParentDetails?: string;
}

export interface FanResult {
  readonly Series: number;
  readonly BladeAngle: number;
  readonly NominalSize: number;
  readonly OperatingPoint: number;
  readonly Efficiency: number;
  readonly OuterBladeDiameter: number;
  readonly NumberOfBladeCuts: number;
  readonly NumberOfBladeExtensions: number;
  readonly FanSpeed: number;
  readonly MotorPower: number;
  readonly TotalPressureDifference: number;
  readonly FanShaftPower: number;
  readonly BackPlate: number;
  readonly ShroudPlate: number;
  readonly Blades: number;
  readonly Hub: number;
  readonly Total: number;
  readonly ShaftPowerWithDustLoad: number;
  readonly GDpow2: number;
  readonly RecommendedMotorPower: number;
  readonly DynamicPressureAtInlet: number;
  readonly DynamicPressureAtOutlet: number;
  readonly DynamicPressureEvaseOutlet: number;
  readonly KinematicViscocity: number;
  readonly DampeningValue: number;
  readonly TipSpeed: number;
  readonly NominalOuterBladeDiameter: number;
  readonly MotorSpeed: number;
  readonly SpeedAtLowestGuaranteePoint: number;
  readonly SpeedAtHighestGuaranteePoint: number;
  readonly BladePassageFrequency1_111: number;
  readonly BladePassageFrequency1_111_2: number;
  readonly BladePassageFrequency1_111_3: number;
  readonly Nomenclature: number;
  readonly StandardBackPlate: number;
  readonly StandardShroud: number;
  readonly StandardBlade: number;
  readonly FlowRate: number;
  readonly UsableTotalPressureDifference: number;
  readonly IVCPosition: number;
  readonly Hertz?: number;
  readonly RestrictedFanSpeed?: number;
}

export type FanSeries = CentrifugalFanSeries &
  ImpellerScantllingsFactors &
  ImpellerScantllings;

export interface CentrifugalFanSeries {
  readonly $id: string;
  A: number;
  B: number;
  FanSeriesId: number;
  Id: number;
  NominalSize: number;
}
export interface ImpellerScantllings {
  readonly $id: string;
  Backplate_mm: number;
  Blade_mm: number;
  Dia_mm: number;
  FanSeriesId: number;
  Id: number;
  Shroud_mm: number;
  Size: number;
}
export interface ImpellerScantllingsFactors {
  readonly $id: string;
  BackplateFactor: number;
  BladeFactor: number;
  HubFactor: number;
  Series: number;
  ShroudFactor: number;
  WeightFactor: number;
}
export interface RpmType {
  readonly $id: string;
  BaringSpan: number;
  Hub: number;
  ImpellerWeight: number;
  Rpm: number;
}
