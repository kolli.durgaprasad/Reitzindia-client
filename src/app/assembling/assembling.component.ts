import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {
  FanProject,
  FanAssembly,
  MultiDisplayControlData
} from '../app.common';

@Component({
  selector: 'app-assembling',
  templateUrl: './assembling.component.html',
  styleUrls: ['./assembling.component.css']
})
export class AssemblingComponent implements OnInit {
  @Input() multiDisplayControlData: MultiDisplayControlData[];
  @Input() fanAssembly: FanAssembly;
  @Output() goToPrevious = new EventEmitter<void>();
  @Output() goToNext = new EventEmitter<void>();

  inletSoundSilensor: MultiDisplayControlData[];
  inletBox: MultiDisplayControlData[];
  otherInletLosses: MultiDisplayControlData[];
  outletSilensor: MultiDisplayControlData[];
  // otherOutlet: MultiDisplayControlData[];
  constructor() {}

  ngOnInit() {
    this.inletSoundSilensor = this.multiDisplayControlData
      ? this.multiDisplayControlData.filter(it => it.ParentId === 10)
      : [];
    this.inletBox = this.multiDisplayControlData
      ? this.multiDisplayControlData.filter(it => it.ParentId === 11)
      : [];
    this.otherInletLosses = this.multiDisplayControlData
      ? this.multiDisplayControlData.filter(it => it.ParentId === 12)
      : [];
    this.outletSilensor = this.multiDisplayControlData
      ? this.multiDisplayControlData.filter(it => it.ParentId === 13)
      : [];
    // this.otherOutlet = this.multiDisplayControlData
    //   ? this.multiDisplayControlData.filter(it => it.ParentId === 12)
    //   : [];
  }

}
