import { Component, OnInit } from '@angular/core';
import {
  FanProject,
  MultiDisplayControlData,
  GasOperatingPoint,
  FanResult
} from '../app.common';
import { FanProjectService } from '../fanproject.service';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  fanresultCount: number;
  fanResult: FanResult[];
  displayFanResult: FanResult[];
  multiDisplayControlData: MultiDisplayControlData[];
  userName: string;
  currentTab = 'Projects';
  displayProjects: FanProject[];
  count: number;
  // curPage = 1;
  pageSize = 10;
  data: FanProject[];
  selectedProject: FanProject;
  filteredProjects: FanProject[];
  filterBy = 'ProjectName';
  isSubmitting = false;
  constructor(
    private service: FanProjectService,
    public ngProgress: NgProgress
  ) {}

  ngOnInit() {
    this.ngProgress.start();
    this.userName = localStorage.getItem('userName');
    this.filteredProjects = [];
    this.displayProjects = [];
    this.service.getAllProjects().then(res => {
      this.ngProgress.done();
      this.data = res;
      this.count = res.length;
      this.displayProjects = this.data.slice(0, this.pageSize);
      this.filteredProjects = this.data;
    });

    this.service
      .getMultiDisplayControlData()
      .then(x => (this.multiDisplayControlData = x));
  }

  onChange($event: any) {
    if ($event.target.value === '') {
      this.displayProjects = this.data.slice(0, this.pageSize);
      this.count = this.data.length;
    } else {
      switch (this.filterBy) {
        case 'ProjectName': {
          this.filteredProjects = this.data.filter(it =>
            it.ProjectName.includes($event.target.value)
          );
          break;
        }
        case 'CustomerName': {
          this.filteredProjects = this.data.filter(it =>
            it.CustomerName.includes($event.target.value)
          );
          break;
        }
        case 'Engineer': {
          this.filteredProjects = this.data.filter(it =>
            it.Engineer.includes($event.target.value)
          );
          break;
        }
        default: {
          this.filteredProjects = this.data.filter(it =>
            it.ProjectName.includes($event.target.value)
          );
          break;
        }
      }

      this.count = this.filteredProjects.length;
      // this.curPage = 1;
      this.displayProjects = this.filteredProjects.slice(0, this.pageSize);
    }
  }

  onChangeFilterByName(val: string) {
    this.filterBy = val;
  }

  onClickProjectSelect(project: FanProject) {
    this.selectedProject = project;
    this.currentTab = 'ProjectData';
  }

  changeTab(tabName: string) {
    this.currentTab = tabName;
  }

  submitFanProject() {
    this.isSubmitting = true;
    this.service.submitFanProject(this.selectedProject).then(res => {
      this.isSubmitting = false;
      this.fanResult = res;
      this.currentTab = 'FanResult';
      this.fanresultCount = this.fanResult.length;
      this.displayFanResult = this.fanResult.slice(0, this.pageSize);
    });
  }
  onAddOperatingPoint() {
    console.log(this.selectedProject.GasOperatingPoints);
    const op: GasOperatingPoint = {
      $id: '',
      Dpt: 0,
      F: 0,
      GasDataId: '',
      Id: '',
      P1: 0,
      Ro: 0,
      T: 0,
      Vi: 0,
      Vp: 0
    };
    const operatingPoints = [...this.selectedProject.GasOperatingPoints, op];
    this.selectedProject = {
      ...this.selectedProject,
      GasOperatingPoints: operatingPoints
    };
  }
}
