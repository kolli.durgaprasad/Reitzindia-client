import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FanProjectService } from '../fanproject.service';
import { FanProject } from '../app.common';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit {
  @Input() displayProjects: FanProject[];
  @Input() count: number;
  curPage = 1;
  pageSize = 10;
  @Input() data: FanProject[];
  @Input() filteredProjects: FanProject[];
  @Input() filterBy: string;

  @Output() changeName = new EventEmitter();
  @Output() changeFilterByName = new EventEmitter<string>();
  @Output() projectSelect = new EventEmitter<FanProject>();

  constructor(private service: FanProjectService) {}

  onChange($event: any) {
    // if ($event.target.value === '') {
    //   this.displayProjects = this.data.slice(0, this.pageSize);
    // } else {
    //   switch (this.filterBy) {
    //     case 'ProjectName': {
    //       this.filteredProjects = this.data.filter(it =>
    //         it.ProjectName.includes($event.target.value)
    //       );
    //       break;
    //     }
    //     case 'CustomerName': {
    //       this.filteredProjects = this.data.filter(it =>
    //         it.CustomerName.includes($event.target.value)
    //       );
    //       break;
    //     }
    //     case 'Engineer': {
    //       this.filteredProjects = this.data.filter(it =>
    //         it.Engineer.includes($event.target.value)
    //       );
    //       break;
    //     }
    //     default: {
    //       this.filteredProjects = this.data.filter(it =>
    //         it.ProjectName.includes($event.target.value)
    //       );
    //       break;
    //     }
    //   }

    //   this.count = this.filteredProjects.length;
    //   this.curPage = 1;
    //   this.displayProjects = this.filteredProjects.slice(0, this.pageSize);
    // }
    this.changeName.emit($event);
    if ($event.target.value !== '') {
      this.curPage = 1;
    }
  }

  onChangeFilterByName($event: any) {
    this.changeFilterByName.emit($event.target.value);
  }

  pagesToShow(count: number) {
    return count < 100 ? Math.ceil(count / 10) : 10;
  }

  onProjectSelect(project: FanProject) {
    this.projectSelect.emit(project);
  }

  ngOnInit() {
    // this.filteredProjects = [];
    // this.displayProjects = [];
    // this.service.getAllProjects().then(res => {
    //   this.data = res;
    //   this.count = res.length;
    //   this.displayProjects = this.data.slice(0, this.pageSize);
    //   this.filteredProjects = this.data;
    // });
  }

  goPrev(go: boolean) {
    this.curPage = this.curPage - this.pageSize;
  }

  goNext(go: boolean) {
    this.curPage = this.curPage + this.pageSize;
  }

  goPage(pageNumber) {
    this.displayProjects = this.filteredProjects.slice(
      (pageNumber - 1) * this.pageSize,
      pageNumber * this.pageSize
    );
    this.curPage = pageNumber;
  }
}
