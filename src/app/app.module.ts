import { LoginService } from './login/login.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { AssemblingComponent } from './assembling/assembling.component';
import { NoiseComponent } from './noise/noise.component';
import { ProjectDataComponent } from './project-data/project-data.component';
import { OperatingPointComponent } from './operating-point/operating-point.component';
import { ControlComponent } from './control/control.component';
import { HttpClientModule } from '@angular/common/http';
import { NgProgressModule } from 'ngx-progressbar';
import { FanProjectService } from './fanproject.service';
import { PaginationComponent } from './pagination/pagination.component';
import { NewProjectComponent } from './new-project/new-project.component';
import { FanresultComponent } from './fanresult/fanresult.component';
import { ImpellerScantllingsComponent } from './impeller-scantllings/impeller-scantllings.component';
import { ImpellerScantllingsEditActualsComponent } from './impeller-scantllings-edit-actuals/impeller-scantllings-edit-actuals.component';
import { ImpellerScantllingsRevisedComponent } from './impeller-scantllings-revised/impeller-scantllings-revised.component';

export const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'pages/projects/list',
    component: LayoutComponent
    // children: [
    //   { path: 'projects/new', component: NewProjectComponent },
    //   { path: 'projects/list', component: ProjectsListComponent },
    //   { path: 'project/data', component: ProjectDataComponent },
    //   { path: 'operatingpoint', component: OperatingPointComponent },
    //   { path: 'control', component: ControlComponent },
    //   { path: 'noise', component: NoiseComponent },
    //   { path: 'assembling', component: AssemblingComponent }
    // ]
  },
  {
    path: 'pages/projects/new',
    component: NewProjectComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    ProjectsListComponent,
    AssemblingComponent,
    NoiseComponent,
    ProjectDataComponent,
    OperatingPointComponent,
    ControlComponent,
    PaginationComponent,
    NewProjectComponent,
    FanresultComponent,
    ImpellerScantllingsComponent,
    ImpellerScantllingsEditActualsComponent,
    ImpellerScantllingsRevisedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    JsonpModule,
    NgProgressModule,
    RouterModule.forRoot(routes)
  ],
  providers: [LoginService, FanProjectService],
  bootstrap: [AppComponent]
})
export class AppModule {}
