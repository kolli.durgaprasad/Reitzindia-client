import { Component, OnInit, Input } from '@angular/core';
import { Noise } from '../app.common';

@Component({
  selector: 'app-noise',
  templateUrl: './noise.component.html',
  styleUrls: ['./noise.component.css']
})
export class NoiseComponent implements OnInit {
  @Input() noiseData: Noise;

  constructor() {}

  ngOnInit() {}
}
