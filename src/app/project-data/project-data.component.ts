import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FanProject } from '../app.common';

@Component({
  selector: 'app-project-data',
  templateUrl: './project-data.component.html',
  styleUrls: ['./project-data.component.css']
})
export class ProjectDataComponent implements OnInit {
  @Input() selectedProject: FanProject;
  @Output() clickNext = new EventEmitter<void>();
  constructor() {}

  ngOnInit() {
    document.getElementById('engineerName').focus();
  }

  handleEnterKey(event, nextTextBox) {
    if (nextTextBox === 'next') {
      document.getElementById(nextTextBox).click();
    } else {
      event.preventDefault();
      document.getElementById(nextTextBox).focus();
    }
  }
}
