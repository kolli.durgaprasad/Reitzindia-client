import { LoginService } from './login.service';
import { Component, OnInit } from '@angular/core';
import { Credentials } from '../app.common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  isSubmitting = false;
  serverError = { show: false, msg: '' };

  constructor(private router: Router, private loginService: LoginService) {}

  onLoginClick(credetital: Credentials) {
    this.router.navigate(['pages', 'projects', 'list']);
    // this.isSubmitting = true;
    // this.loginService
    //   .postCredentials(credetital)
    //   .then(res => {
    //     localStorage.setItem('userName', res.user.UserName);
    //     this.isSubmitting = false;
    //     this.serverError = { show: false, msg: '' };
    //     this.router.navigate(['pages', 'projects', 'list']);
    //   })
    //   .catch(err => {
    //     this.isSubmitting = false;
    //     const { status } = err;
    //     switch (status) {
    //       case 401:
    //         this.serverError = { show: true, msg: 'Invalid Credentials' };
    //         break;

    //       case 400:
    //         this.serverError = { show: true, msg: 'Bad Request' };
    //         break;

    //       default:
    //         this.serverError = {
    //           show: true,
    //           msg: 'Internal Server Error, Please try after sometime.'
    //         };
    //         break;
    //     }
    //   });
  }
}
