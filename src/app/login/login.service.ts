import { Credentials, LoginResponse } from './../app.common';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginService {
  readonly url: string;
  constructor(private http: Http) {}

  postCredentials(data: Credentials): Promise<LoginResponse> {
    const { userName: UserName, password: Password } = data;
    console.log({
      UserName: data.userName,
      Password: data.password
    });
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers });
    return this.http
      .post(
        'http://183.82.2.110/ReitzService/api/Account/Login',
        JSON.stringify({
          UserName: data.userName,
          Password: data.password
        }),
        options
      )
      .toPromise()
      .then(r => {
        return r.json() as LoginResponse;
      });
  }
}

