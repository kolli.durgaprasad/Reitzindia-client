import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpellerScantllingsEditActualsComponent } from './impeller-scantllings-edit-actuals.component';

describe('ImpellerScantllingsEditActualsComponent', () => {
  let component: ImpellerScantllingsEditActualsComponent;
  let fixture: ComponentFixture<ImpellerScantllingsEditActualsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpellerScantllingsEditActualsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpellerScantllingsEditActualsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
