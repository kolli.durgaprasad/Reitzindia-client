import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FanresultComponent } from './fanresult.component';

describe('FanresultComponent', () => {
  let component: FanresultComponent;
  let fixture: ComponentFixture<FanresultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FanresultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FanresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
