import { Component, OnInit, Input } from '@angular/core';
import { FanResult } from '../app.common';

@Component({
  selector: 'app-fanresult',
  templateUrl: './fanresult.component.html',
  styleUrls: ['./fanresult.component.css']
})
export class FanresultComponent implements OnInit {
  selectedResult: FanResult;
  @Input() fanResult: FanResult[];
  @Input() displayFanResult: FanResult[];
  curPage = 1;
  pageSize = 10;
  @Input() fanresultCount: number;
  constructor() {}

  ngOnInit() {}

  fanSettings(f: FanResult) {
    this.selectedResult = f;
    console.log(f);
  }

  pagesToShow(count: number) {
    return count < 100 ? Math.ceil(count / 10) : 10;
  }

  goPrev(go: boolean) {
    if (this.curPage > 1) {
      this.curPage = this.curPage - this.pageSize;
    }
  }

  goNext(go: boolean) {
    this.curPage = this.curPage + this.pageSize;
  }

  goPage(pageNumber) {
    this.displayFanResult = this.fanResult.slice(
      (pageNumber - 1) * this.pageSize,
      pageNumber * this.pageSize
    );
    this.curPage = pageNumber;
  }
}
