import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { MaterialDriveControl } from '../app.common';
interface Selections {
  drive: string;
  hertz: string;
}
@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})
export class ControlComponent implements OnInit {
  selections: Selections = { drive: 'vBelt', hertz: 'Standard' };
  @Input() isSubmitting: boolean;
  // temporary code

  @Input() materialControl: MaterialDriveControl;
  @Output() goToPrevious = new EventEmitter<void>();
  @Output() goToNext = new EventEmitter<void>();
  @Output() submitFanProject = new EventEmitter<void>();
  singleFanType = [
    { value: 'KXE', name: 'KXE' },
    { value: 'KBE', name: 'KBE' },
    { value: 'KXA', name: 'KXA' },
    { value: 'KBA', name: 'KBA' },
    { value: 'MAE', name: 'MAE' },
    { value: 'MXE', name: 'MXE' },
    { value: 'RBC', name: 'RBC' },
    { value: 'RGE', name: 'RGE' },
    { value: 'RHE', name: 'RHE' }
  ];
  doubleFanType = [
    { value: 'KBX', name: 'KBX' },
    { value: 'KBZ', name: 'KBZ' },
    { value: 'KXZ', name: 'KXZ' },
    { value: 'RGZ', name: 'RGZ' }
  ];

  constructor() {}

  ngOnInit() {
    console.log(this.materialControl);
  }
  widthOnClick(num: number) {
    this.materialControl.Width = num;
  }
  setDrive(value: string): void {
    this.selections.drive = value;
  }

  onChangeMechanicalDesignTemp($event) {
    this.materialControl.MechanicalDesignTemperature = $event.target.value;
    if ($event.target.value > 350) {
      this.materialControl.StandardImpellerMaterial = true;
    } else {
      this.materialControl.StandardImpellerMaterial = false;
    }
  }
}
