import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpellerScantllingsRevisedComponent } from './impeller-scantllings-revised.component';

describe('ImpellerScantllingsRevisedComponent', () => {
  let component: ImpellerScantllingsRevisedComponent;
  let fixture: ComponentFixture<ImpellerScantllingsRevisedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpellerScantllingsRevisedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpellerScantllingsRevisedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
